from django import forms
from django.shortcuts import render, get_object_or_404
from django.forms.widgets import *

from .models import Activity

class PostForm(forms.ModelForm):

	class Meta:
		model = Activity
		fields = ('activity_name', 'category', 'place', 'start_date', 'description')
		labels = {
		'activity_name':'Nama Aktivitas',
		'category':'Kategori',
		'place':'Tempat',
		'start_date':'Tanggal Mulai'
		}
		widgets = {
		'activity_name': forms.TextInput(attrs={'class':'form-control'}),
		'category': forms.TextInput(attrs={'class':'form-control'}),
		'place': forms.TextInput(attrs={'class':'form-control'}),
		'start_date': forms.DateTimeInput(attrs={'class':'form-control', 'placeholder':'format: yyyy-mm-dd HH:mm'}),
		'description': forms.Textarea(attrs={'class':'form-control'}),
		}