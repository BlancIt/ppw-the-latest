from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
	path('about/', views.about, name='about'),
	path('special/', views.special, name='special'),
	path('activity/', views.activity, name='activity'),
	path('activity/list/', views.activity_list, name='activity_list'),
	path('activity/<int:pk>/', views.activity_info, name='activity_info'),
	path('activity/confirm_delete/', views.activity_confirm_delete, name='activity_confirm_delete'),
	path('activity/confirm_delete/activity_delete/', views.activity_delete, name='activity_delete'),
	path('activity/add/', views.activity_add, name='activity_add'),
	path('register/', views.register, name='register'),
	path('registered/', views.registered, name='registered'),
]