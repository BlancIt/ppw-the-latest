from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Activity
from .forms import PostForm
from django.utils import timezone
from django.shortcuts import redirect

site = ['HOMEPAGE', 'ABOUT ME', 'SPECIAL', 'REGISTER', 'AKTIVITAS']
nama = ['Muhammad Haikal Baihaqi', 'Suci Fadhilah']
umur = ['19 Tahun', '24 Tahun']
hobi = ['Bermain, membaca', 'Membaca']
favorit = ['"Progress!"', '“Maka Sesungguhnya bersama kesulitan ada kemudahan.”']

def index(request):
	response = {'sitename': site[0]}
	return render(request, 'index.html', response)
	
def about(request):
	response = {'sitename': site[1], 'name': nama[0], 'age': umur[0], 'hobby': hobi[0], 'quote': favorit[0]}
	return render(request, 'about.html', response)

def special(request):
	response = {'sitename': site[2], 'name': nama[1], 'age': umur[1], 'hobby': hobi[1], 'quote': favorit[1]}
	return render(request, 'special.html', response)
	
def activity(request):
	response = {'sitename': site[4]}
	return render(request, 'act_index.html', response)
	
def activity_list(request):
	activities = Activity.objects.filter(start_date__lte=timezone.now()).order_by('start_date')
	return render(request, 'act_list.html', {'activities': activities})
	
def activity_confirm_delete(request):
	response = {'sitename': site[4]}
	return render(request, 'act_confirm_del.html', response)
	
def activity_delete(request):
	if request.method == "POST":
		Activity.objects.all().delete()
		return redirect('activity')
	
def activity_info(request, pk):
    activities = get_object_or_404(Activity, pk=pk)
    return render(request, 'act_info.html', {'activities': activities})
	
def activity_add(request):
	if request.method == "POST":
		form = PostForm(request.POST)
		if form.is_valid():
			activity = form.save(commit=False)
			activity.author = request.user
			activity.save()
			return redirect('activity_info', pk=activity.pk)
	else:
		form = PostForm()
	return render(request, 'act_add.html', {'form': form})
	
	

def register(request):
	response = {'sitename': site[3]}
	return render(request, 'register.html', response)	
	
def registered(request):
	response = {'sitename': site[3]}
	return render(request, 'registered.html', response)