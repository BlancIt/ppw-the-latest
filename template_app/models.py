from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Activity(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    activity_name = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
    place = models.CharField(max_length=200)
    description = models.TextField()
    start_date = models.DateTimeField()

    def submit(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.activity_name